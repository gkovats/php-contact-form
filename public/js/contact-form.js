toastr.options.closeButton = true;

//Wait for the DOM to be ready
$(function() {

	var contactForm = $("form[name='contact']")
	contactForm.validate({
        // Specify validation rules
        rules: {
            fname: {
            	required: true,
            	maxlength: 200
            },
            lname: {
            	required: true,
            	maxlength: 200
            },
            email: {
                required: true,
                email: true
            },
            phone: {
            	maxlength: 20
            },
            message: {
            	required: true
            }
        },

        // Specify validation error messages
        messages: {
            firstname: "Please enter your first name",
            lastname: "Please enter your last name",
            email: "Please enter a valid email address",
            message: "Please enter a message"
        },
        
        submitHandler: function(form) {
            
        	// REALY quick and dirty Ajax form submit
        	var formObj = contactForm.serializeArray(),
        		data	= {},
        		i		= 0,
        		url		= "/api/contact";

			for (i = 0; i < formObj.length; i++){
			    data[formObj[i]['name']] = formObj[i]['value'];
			}
			
			// If we're using the PHP test server (port 9999)
			// change url
			if (document.location.port) {
				url = "/post.php";
			}
			
			// Simple Ajax post
			$.ajax({
				data: data,
				url: url,
				method: "POST",
				dataType: "json"
			}).done(function( data ) {
				if (data.success) {
					toastr.info('Thanks! We\'ll follow up with you shortly!!');
					contactForm.find('input, textara, button').prop('disabled', true);
					$('section#contact').fadeOut();
				} else {
					toastr.error('There was an error with your submission. ' + data.responseJSON.message);
				}
		    }).fail(function(data){
		    	toastr.error('There was an error with your submission. ' + data.responseJSON.message);
		    });
			
			
        }
    });
});

