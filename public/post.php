<?php 

/*
 * I'd already done the SlimApp routes found in 
 * api.php, but had to slap this together for a
 * single purpose endpoint
 */

require "../vendor/autoload.php";
require "../bootstrap.php";

$db = \utilities\DB::getDB();
$data = $_REQUEST;
$contact = \controllers\Contact::getInstance();
$responseObj = $contact->insert($data);
$status = $responseObj->success ? 200 : 400;
http_response_code($status);

die(json_encode($responseObj->toArray()));


