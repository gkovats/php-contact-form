

<h2>Howday, <?php echo $data['fname']; ?>!</h2>

<p> We've received your entry in our feedback system with the following information:</p>

<ul>
	<li><b>Name</b>: <?php echo $data['fname'] ?? ''; echo " " . $data['lname'] ?? ''; ?></li>
	<li><b>Phone</b>: <?php echo $data['phone'] ?? ''; ?></li>
	<li><b>Email</b>: <?php echo $data['email'] ?? ''; ?></li>
	<li><b>Message</b>: <?php echo $data['message'] ?? ''; ?></li>
</ul>

<p> Thanks! </p>

