
Howday, <?php echo $data['fname']; ?>!

We've received your entry in our feedback system with the following information:

* Name: <?php echo $data['fname'] ?? ''; echo " " . $data['lname'] ?? ''; ?>
* Phone: <?php echo $data['phone'] ?? ''; ?>
* Email: <?php echo $data['email'] ?? ''; ?>
* Message: <?php echo $data['message'] ?? ''; ?>

Thanks!

