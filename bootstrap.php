<?php 

require "vendor/autoload.php";

define('ROOT_PATH', realpath(dirname(__FILE__)) . '/' );

// Autoload Classes
spl_autoload_register(function ($class) {
    $path = str_replace("\\", "/", $class);
    if (file_exists(ROOT_PATH . 'classes/' . $path . '.php')) {
        include_once ROOT_PATH . 'classes/' .  $path . '.php';
    }
});
    