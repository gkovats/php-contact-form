<?php

/**
 * @covers \models\Contact
 */
final class ContactTest extends \PHPUnit\Framework\TestCase
{

    public function testNewContactValidation()
    {
        $data = array(
            "fname" => "Joe",
            "lname" => "Cool",
            "email" => "whatup@test.com",
            "bademail" => "whatup",
            "phone" => "234.343.2343",
            "message" => "Message"
        );
        
        $contact = new \models\Contact();
        $contact->fname = $data['fname'];
        $this->assertEquals($contact->fname, $data['fname'], "Adding first name");
        
        // attempt to save
        try {
            $contact->save();
        } catch (\exceptions\AppException $e) {
            $this->assertEquals($e->getCode(), \models\Contact::ERR_NO_NAME, "Can't save without a last name");
        }
        
        // add last name
        $contact->lname = $data['lname'];
        $this->assertEquals($contact->lname, $data['lname']);
        
        // attempt to save
        try {
            $contact->save();
        } catch (\exceptions\AppException $e) {
            $this->assertEquals($e->getCode(), \models\Contact::ERR_NO_EMAIL, "Can't save without an email");
        }
        
        // add email
        $contact->email = $data['bademail'];
        $this->assertEquals($contact->email, $data['bademail'], "Bad email added");
        
        // attempt to save
        try {
            $contact->save();
        } catch (\exceptions\AppException $e) {
            $this->assertEquals($e->getCode(), \models\Contact::ERR_BAD_EMAIL, "Email in bad format");
        }
        
        // add email
        $contact->email = $data['email'];
        $this->assertEquals($contact->email, $data['email'], "Good email added.");
        
        // attempt to save
        try {
            $contact->save();
        } catch (\exceptions\AppException $e) {
            $this->assertEquals($e->getCode(), \models\Contact::ERR_NO_MESSAGE, "Can't save without message");
        }
        
        // add message
        $contact->message = $data['message'];
        $this->assertEquals($contact->message, $data['message'], "Message was added");
        
        // attempt to save
        try {
            $contact->save();
        } catch (\exceptions\AppException $e) {
            $this->assertEquals($e->getCode(), \models\Contact::ERR_BAD_EMAIL);
            $this->assertNull($e, "Should not get an exception saving at this point.");
        }
        
    }
}

