<?php
namespace utilities;

/**
 * SlimApp container
 * 
 * @author gkovats
 */
class SlimApp
{

    /**
     * Internal instance holder
     *
     * @var object
     */
    private static $instance;

    /**
     * Save settings for Slim
     *
     * @var array
     */
    private $settings = array(
        "routerCacheFile" => false,
        "displayErrorDetails" => false
    );

    /**
     * Holds Slim app instance
     *
     * @var object
     */
    private static $app;

    /**
     * Fetch Singleton instance
     *
     * @return object|\utilities\SlimApp
     */
    public static function getInstance()
    {
        if (! isset(self::$instance)) {
            self::$instance = new SlimApp();
        }
        return self::$instance;
    }

    /**
     * Constructor - starts the routes
     */
    private function __construct()
    {
        $this->settings = \utilities\Config::getConfig()['slim'];
        self::$app = new \Slim\App([
            "settings" => $this->settings
        ]);
        
        // fetch instance of DB
        \utilities\DB::getDB();
        
        // load routes
        \utilities\Routes::getInstance(self::$app);
        
        return self::$app;
    }
}