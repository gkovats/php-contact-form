<?php
namespace utilities;

/**
 * Mail utlitiy
 *
 * @author gkovats
 */
class Mail extends \PHPMailer
{

    const ERR_MAIL_TEMPLATE_NOT_FOUND   = 60;
    const ERR_MAIL_SEND_FAILURE         = 65;
    
    /**
     * PHP Mailer object
     *
     * @var \PHPMailer
     */
    private $phpmail;
    
    /**
     * Constructor - starts the routes
     */
    public function __construct()
    {
        $config = \utilities\Config::getConfig();
        parent::__construct();
        
        /*
         * Configuration informed by config ini
        $this->isSMTP();                                      // Set mailer to use SMTP
        $this->Host = 'smtp1.example.com;smtp2.example.com';  // Specify main and backup SMTP servers
        $this->SMTPAuth = true;                               // Enable SMTP authentication
        $this->Username = 'user@example.com';                 // SMTP username
        $this->Password = 'secret';                           // SMTP password
        $this->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
        $this->Port = 587;                                    // TCP port to connect to
        */
    }

    /**
     * Render a template for the body of the email
     * 
     * @param string $template   Name of template to use
     * @param array  $data       Data for tempalte
     * @param bool   $useAltBody Flag to use AltBody instead
     * @throws \exceptions\AppException
     */
    public function useTemplate(string $template, array $data, bool $useAltBody = false)
    {
        // Here you'd want to add more logic to lock down what templates
        // can be supplied, and make sure no one slips in an injection
        $file = ROOT_PATH . "templates/$template.php";
        if (!file_exists($file)) {
            throw new \exceptions\AppException("Mail template file '$file' not found.", self::ERR_MAIL_TEMPLATE_NOT_FOUND);
        }
        ob_start();
        include $file;
        if ($useAltBody) {
            $this->AltBody = ob_get_clean();
        } else {
            $this->Body = ob_get_clean();
        }
        ob_end_clean();
    }
    
    /**
     * Overload of send
     * {@inheritDoc}
     * @see PHPMailer::send()
     */
    public function send() {
        if (!parent::send()) {
            throw new \exceptions\AppException("Issue sending mail: ". $this->ErrorInfo, self::ERR_MAIL_SEND_FAILURE);
        }
    }
    
}