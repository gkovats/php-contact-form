<?php
namespace utilities;

/**
 * Routes handler
 *
 * @author gkovats
 */
class Routes
{

    /**
     * Internal instance holder
     *
     * @var object
     */
    private static $instance;

    /**
     * Fetch Singleton instance
     *
     * @param \Slim\App $app
     *            Instance of SlimApp
     */
    public static function getInstance(\Slim\App $app)
    {
        if (! isset(self::$instance)) {
            self::$instance = new Routes($app);
        }
        return self::$instance;
    }

    /**
     * Constructor - starts the routes
     *
     * @param \Slim\App $app
     *            Instance of SlimApp
     */
    private function __construct(\Slim\App $app)
    {
        
        // Attach controllers
        $container = $app->getContainer();
        $container['contact'] = function ($container) {
            return \controllers\Contact::getInstance();
        };
        
        // Contact insert
        $app->post('/api/contact', function ($request, $response) {
            $data = $request->getParams();
            $responseObj = $this->contact->insert($data);
            $status = $responseObj->success ? 200 : 400;
            return $response->withJson($responseObj->toArray(), $status);
        });
        
        // Contact update
        $app->put('/api/contact', function ($request, $response) {
            $data = $request->getParams();
            $id = intval($data['id'] ?? 0);
            $responseObj = $this->contact->update($data, $id);
            $status = $responseObj->success ? 200 : 400;
            return $response->withJson($responseObj->toArray(), $status);
        });
        
        // Finally run the app
        $app->run();
    }
}