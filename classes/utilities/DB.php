<?php
namespace utilities;

/**
 * DB container
 *
 * @author gkovats
 */
class DB
{
    
    /**
     * Internal instance holder
     *
     * @var object
     */
    private static $instance;
    
    /**
     * DB object
     *
     * @var \newAdoConnection
     */
    private static $db;
    
    /**
     * Fetch Singleton instance
     *
     * @param array $config DB Configuration
     * @return object|\utilities\DB
     */
    public static function getDB($config = null)
    {
        if (! isset(self::$instance)) {
            self::$instance = new DB($config);
        }
        return self::$db;
    }
    
    /**
     * Constructor - starts the routes
     * 
     * @param array $config DB Configuration
     */
    private function __construct()
    {
        $config = \utilities\Config::getConfig()['db'];
        $driver = 'mysqli';
        self::$db = \newAdoConnection($driver);
        self::$db->connect($config['host'], $config['user'], $config['pass'], $config['dbname']);
        // @TODO: check connection, and throw exception if no
        /*
        if (!self::$db->isConnected()) {
            throw \DBException;
        }
        */
    }

}