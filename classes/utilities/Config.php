<?php
namespace utilities;

/**
 * Config class to load app config
 *
 * @author gkovats
 */
class Config
{

    /**
     * Internal instance holder
     *
     * @var object
     */
    private static $instance;

    /**
     * Holds data for Config
     *
     * @var array
     */
    private static $config = array(
        "db" => "test"
    );

    /**
     * Fetch Singleton instance
     *
     * @return array Config data array
     */
    public static function getConfig(string $configPath = NULL)
    {
        if (! isset(self::$instance)) {
            self::$instance = new Config();
        }
        return self::$config;
    }

    /**
     * Constructor - starts the routes
     */
    private function __construct(string $configPath = NULL)
    {
        if (is_null($configPath)) {
            $configPath = ROOT_PATH . "config/config.ini";
        }
        self::$config = array_merge(self::$config, parse_ini_file($configPath, true, INI_SCANNER_TYPED));
    }

    /**
     * Fetching a config value
     *
     * @param string $property
     */
    public function __get(string $property)
    {
        if (array_key_exists($property, self::$config)) {
            return self::$config[$property];
        }
        return false;
    }
}