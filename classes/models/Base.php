<?php
namespace models;

/**
 * SlimApp container
 *
 * @author gkovats
 */
class Base
{

    const ERR_DB_SAVE_ERROR = 41;

    const INT = "i";

    const STRING = "s";

    const DATE = "d";

    const ERR_NOT_UPDATED = 301;

    /**
     * Save settings for Slim
     *
     * @var \utilities\DB
     */
    protected $db;

    /**
     * Name of table in DB
     *
     * @var string
     */
    protected $table;

    /**
     * Has this object been updated?
     *
     * @var boolean
     */
    protected $isDirty = false;

    /**
     * List of fields in DB for object
     *
     * @var array
     */
    protected $fields = array(
        'id' // all models should have an id
                 // we'd add the other standard fields here like
                 // last mod date and create date
    );

    /**
     * Holds to values for the model object
     *
     * @var array
     */
    protected $data = array();

    /**
     * ID of object in DB
     *
     * @var integer
     */
    protected $id = 0;

    /**
     * Constructor - starts the routes
     *
     * @param int $id
     *            [optional] If supplied, will load object by id
     */
    public function __construct($id = null)
    {
        $this->db = \utilities\DB::getDB();
        
        // stub out data
        foreach ($this->fields as $field => $type) {
            $this->data[$field] = null;
        }
        
        if (is_int($id) && $id > 0) {
            $this->getById($id);
        }
    }

    /**
     * Return fields
     *
     * @return array
     */
    public function getFields()
    {
        return $this->fields;
    }

    /**
     * Magic getter for model properties
     *
     * @param string $field
     * @return mixed|boolean
     */
    public function __get(string $field)
    {
        // if specific getter is available, trigger it now
        if (is_callable(array(
            $this,
            'get' . ucfirst($field)
        ))) {
            $method = 'get' . ucfirst($field);
            return $this->$method($value);
        }
        
        // if a field exists, return it's value
        if (in_array($field, array_keys($this->fields))) {
            return $this->data[$field];
        }
        return false;
    }

    /**
     * Magic setting for model properties
     *
     * @param string $field
     * @param mixed $value
     * @return boolean
     */
    public function __set(string $field, $value)
    {
        // must be a valid field
        if (! in_array($field, array_keys($this->fields))) {
            return false;
        }
        
        // cast the value
        switch ($this->fields[$field]) {
            case self::INT:
                $value = intval($value);
                break;
            case self::STRING:
                $value = (string) $value;
                break;
            // would handle date, char and others,
            // but don't have them here
        }
        
        // if specific setter is available, trigger it now
        if (is_callable(array(
            $this,
            'set' . ucfirst($field)
        ))) {
            $method = 'set' . ucfirst($field);
            $this->$method($value);
        } else {
            $this->data[$field] = $value;
        }
        $this->isDirty = true;
    }

    /**
     * Get an object by id
     *
     * @param int $id
     */
    public function getById(int $id)
    {
        // I didn't see where Adodb offers a prepare / clean
        // method for building queries - for sake of time,
        // assuming this will suffice.
        $sql = "select * from " . $this->table . " where id = $id";
        $result = $this->db->getRow($sql);
        
        if (is_array($result)) {
            $this->id = $id;
            $this->fromArray($result);
            $this->isDirty = false;
            return true;
        } else {
            return false;
        }
    }

    /**
     * Given an array of data, populate this object
     *
     * @param array $data
     */
    public function fromArray(array $data)
    {
        foreach ($data as $key => $value) {
            
            $key = trim($key);
            
            // field needs to be in object
            if (! in_array($key, array_keys($this->fields))) {
                continue;
            }
            // cannot update id
            if ($key == 'id') {
                continue;
            }
            $this->$key = $value;
        }
    }

    /**
     * Fetch object as an array
     *
     * @return array
     */
    public function toArray()
    {
        return $this->data;
    }

    /**
     *
     * @param bool $force
     * @return boolean
     */
    public function save(bool $force = false)
    {
        // If no updates have been made, don't save
        if (! $this->isDirty && $force == false) {
            throw new \exceptions\AppException('Aborting save, no changes made.', self::ERR_NOT_UPDATED);
        }
        // fetch everything but the id
        $data = $this->data;
        unset($data['id']);
        
        // if id is null or < 1 this is an insert
        $result = false;
        
        if (is_null($this->id) || $this->id < 1) {
            $result = $this->db->autoExecute($this->table, $data, 'INSERT');
            if ($result) {
                $id = $this->db->Insert_ID();
                $this->id = intval($id);
            }
        } else {
            $result = $this->db->autoExecute($this->table, $data, 'UPDATE', 'id = ' . $this->id);
        }
        
        if (! $result) {
            throw new \exceptions\AppException("Error saving record to DB: " . $this->db->ErrorMsg(), self::ERR_DB_SAVE_ERROR);
        }
        return $result;
    }
}