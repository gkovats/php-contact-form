<?php
namespace models;

/**
 * SlimApp container
 *
 * @author gkovats
 */
class Contact extends \models\Base
{

    const ERR_NO_EMAIL = 501;

    const ERR_BAD_EMAIL = 505;

    const ERR_NO_NAME = 510;

    const ERR_NO_MESSAGE = 515;
    
    /**
     * List of fields in DB for object
     *
     * @var array
     */
    protected $fields = array(
        'id' => self::INT,
        'fname' => self::STRING,
        'lname' => self::STRING,
        'email' => self::STRING,
        'message' => self::STRING,
        'phone' => self::STRING
    );

    /**
     * Setting the table name
     *
     * @var string
     */
    protected $table = 'contact';

    /**
     * Constructor - starts the routes
     *
     * @param int $id
     *            [optional] If supplied, will load object by id
     *            
     */
    public function __construct($id = null)
    {
        parent::__construct($id);
    }

    /**
     * Overload of parent save
     * 
     * {@inheritdoc}
     * @see \models\Base::save()
     */
    public function save(bool $force = false)
    {
        
        // Must have an email address
        $email = $this->email;
        $message = $this->message;
        $fname = $this->fname;
        $lname = $this->lname;
        if (empty($fname) || empty($lname)) {
            throw new \exceptions\AppException('Contact must have a first and last name.', self::ERR_NO_NAME);
        }
        if (empty($email)) {
            throw new \exceptions\AppException('Contact must have an email address.', self::ERR_NO_EMAIL);
        }
        if (! filter_var($email, FILTER_VALIDATE_EMAIL)) {
            throw new \exceptions\AppException('Contacts must have a valid email address.', self::ERR_BAD_EMAIL);
        }
        if (empty($message)) {
            throw new \exceptions\AppException('Contact must have a message.', self::ERR_NO_MESSAGE);
        }
        
        return parent::save($force);
    }
}