<?php
namespace controllers;

/**
 * Contact controller
 *
 * @author gkovats
 */
class Contact
{

    const ERR_NO_ID_FOR_UPDATE = 400;

    const ERR_NOT_FOUND_BY_ID = 405;

    /**
     * Internal instance holder
     *
     * @var object
     */
    private static $instance;

    /**
     * Config
     */
    private $config = array();

    /**
     * Fetch Singleton instance
     *
     * @param \Slim\App $app
     *            Instance of SlimApp
     */
    public static function getInstance()
    {
        if (! isset(self::$instance)) {
            self::$instance = new Contact();
        }
        return self::$instance;
    }

    /**
     * Constructor
     */
    private function __construct()
    {
        $this->config = \utilities\Config::getConfig();
    }

    /**
     * Insert new record, or update an existing one given an id
     *
     * @param array $data
     *            Data for creating a new contact entry
     * @return \controllers\RestResponse
     */
    public function upsert(array $data, int $id = 0)
    {
        $response = new \controllers\RestResponse(false);
        $response->unit = "contact";
        // create new contact or fetch existing one by id
        $contact = new \models\Contact($id);
        
        // if id isn't set, we couldn't find item by id
        $isUpdate = ($id > 0 && $contact->id !== $id);
        if ($isUpdate) {
            $response->code = self::ERR_NOT_FOUND_BY_ID;
            $response->message = "Contact not found by id $id.";
            return $response;
        }
        
        try {
            $contact->fromArray($data);
            $success = $contact->save();
            $response->success = true;
            $response->message = "Contact successfully " . ($isUpdate ? "updated" : "added");
        } catch (\exceptions\AppException $e) {
            $response->code = $e->getCode();
            $response->message = $e->getMessage();
        }
        
        return $response;
    }

    /**
     * Update an existing record.
     * Forces the ID
     *
     * @param array $data
     * @param int $id
     * @return \controllers\RestResponse
     */
    public function update(array $data, int $id)
    {
        // for updates, we _need_ an id
        if ($id < 1) {
            $response = new \controllers\RestResponse(false, self::ERR_NO_ID_FOR_UPDATE, "Updates must have a valid ID.", "contact");
            return $response;
        }
        return $this->upsert($data, $id);
    }

    /**
     * Update an existing record.
     * Kicks off email after successful save
     *
     * @param array $data
     * @param int $id
     * @return \controllers\RestResponse
     */
    public function insert(array $data)
    {
        $response = $this->upsert($data);
        
        // If the insert is successful, send out two emails
        if ($response->success) {
            $response->message = "Contact successfully added.";
            try {
                $email = new \utilities\Mail();
                $email->useTemplate('contactNew', $data);
                $email->useTemplate('contactNewTxt', $data, true);
                $email->addAddress($data['email'], $data['fname'] . " " . $data['lname']);
                if (isset($this->config['mail']['ownerEmail'])) {
                    $email->addBCC($this->config['mail']['ownerEmail']);
                }
                $email->Subject = "Contact request";
                $email->send();
            } catch (\exceptions\AppException $e) {
                // @TODO Log email failure
                error_log ("Email failure: ". $e->getMessage() . ": " . $e->getCode());
            }
            
        }
        return $response;
    }
}