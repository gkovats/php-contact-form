<?php
namespace controllers;

class RestResponse
{
    
    public function __construct(bool $success = false, int $code = 0, string $unit = "", string $message = "") {
        $this->success = $success;
        $this->code = $code;
        $this->unit = $unit;
        $this->message = $message;
    }
    
    public $success = false;
    
    public $code = 0;

    public $unit = "";
    
    public $message = "";

    /**
     * Return this object in array form
     * @return string[]|number[]
     */
    public function toArray() {
        return array(
            "success" => $this->success,
            "code" => $this->code,
            "unit" => $this->unit,
            "message" => $this->message
        );
    }
    
}